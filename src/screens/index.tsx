import { StatusBar } from "expo-status-bar";
import React from "react";
import { View } from "react-native";
import { Colors } from "../styles";
import GarageScreen from "./Garage/GarageScreen";

const Garage = () => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.backgroundColor,
        alignItems: "flex-start",
        justifyContent: "center",
      }}
    >
      <StatusBar style="auto" />
      <GarageScreen />
    </View>
  );
};

export default Garage;