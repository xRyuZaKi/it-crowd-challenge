import React, { useCallback, useEffect, useState } from "react";
import { View, Text, FlatList, ActivityIndicator, Alert } from "react-native";

import { styles } from "./styles";
import { useRequest } from "../../hooks/useRequest";
import { Item } from "../../models/CarInterface";
import { AxiosRequestConfig } from "axios";
import { useDispatch } from "react-redux";
import { checkFavorite as checkFavoriteRedux } from "../../../redux/slices/carsSlice";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { RootState } from "../../../redux/store";
import { ItemCar } from "../../components/ItemCar";
import AsyncStorage from "@react-native-async-storage/async-storage";

const GarageScreen = () => {
  const { isLoading, hasError, isSuccess, data, api } = useRequest();
  const [cars, setCars] = useState<Item[]>([]);
  const [favoriteCars, setFavoriteCars] = useState<Item[]>([]);

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const dataCars = useSelector((state: RootState) => state.cars);
  const { cars: carsRedux } = dataCars;

  useEffect(() => {
    const getData = async () => {
      const values = await AsyncStorage.getItem("favoriteCars");
      const response = values != null ? JSON.parse(values) : null;

      if (response != null) {
        setFavoriteCars(response);
      } else {
        setFavoriteCars(carsRedux);
      }
    };

    getData();
  }, []);

  useFocusEffect(
    useCallback(() => {
      getCars();
    }, [])
  );

  useEffect(() => {
    if (!isLoading && !hasError && isSuccess) {
      const response: Item[] = data.items as Item[];
      setCars(response);
    } else if (!isLoading && hasError && !isSuccess) {
      Alert.alert("Error", "Ha ocurriendo un error intente nuevamente...");
    }
  }, [isLoading]);

  const getCars = async () => {
    const config: AxiosRequestConfig = {
      method: "GET",
    };
    api.sendRequest("/", config);
  };

  const checkFavorite = (index: number) => {
    let tmp = [...cars];
    let tmpFavoriteCars = [...favoriteCars];

    const idCar = tmp[index].id;
    let indexFavoriteCar = tmpFavoriteCars.findIndex(
      (value) => value.id === idCar
    );

    if (indexFavoriteCar === -1) {
      tmpFavoriteCars.push(tmp[index]);
    } else {
      tmpFavoriteCars.splice(indexFavoriteCar, 1);
    }

    setFavoriteCars(tmpFavoriteCars);
    dispatch(checkFavoriteRedux(tmpFavoriteCars));
    setCars(tmp);

    storeData(tmpFavoriteCars);
  };

  const storeData = async (values: Item[]) => {
    await AsyncStorage.setItem("favoriteCars", JSON.stringify(values));
  };

  const isFavoriteCar = (id: string) => {
    const index = favoriteCars.findIndex((value) => value.id === id);

    return index !== -1;
  };

  const renderItem = (item: Item, index: number) => {
    return (
      <View style={{ marginVertical: 10 }}>
        <ItemCar
          item={item}
          checkFavorite={() => checkFavorite(index)}
          isFavoriteCar={() => isFavoriteCar(item.id)}
          index={index}
        />
      </View>
    );
  };

  return (
    <>
      {isLoading ? (
        <View
          style={{ flex: 1, justifyContent: "center", alignSelf: "center" }}
        >
          <ActivityIndicator color={"red"} size={30} />
        </View>
      ) : (
        <View style={styles.container}>
          <FlatList
            data={cars}
            renderItem={({ item, index }) => renderItem(item, index)}
            keyExtractor={(item) => item.id.toString()}
            ListHeaderComponent={
              <View style={{ marginTop: 10 }}>
                <Text style={{ fontWeight: "bold", fontSize: 35 }}>Garage</Text>
              </View>
            }
          />
        </View>
      )}
    </>
  );
};

export default GarageScreen;
