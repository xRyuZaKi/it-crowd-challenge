import React from "react";
import { View } from "react-native";
import { useSelector } from "react-redux";
import { RootState } from "../../../redux/store";
import { styles } from "./styles";
import { useRoute } from "@react-navigation/native";
import { Item } from "../../models/CarInterface";
import { ItemCar } from "../../components/ItemCar";

const DetailScreen = () => {
  const data = useSelector((state: RootState) => state.cars);
  const { cars } = data;

  const route = useRoute();

  const item = route.params as Item;

  const isFavoriteCar = (id: string) => {
    const index = cars.findIndex((value) => value.id === id);

    return index !== -1;
  };

  return (
    <View style={styles.container}>
      <ItemCar
        checkFavorite={() => console.log("")}
        index={0}
        isFavoriteCar={() => isFavoriteCar(item.id)}
        item={item}
      />
    </View>
  );
};

export default DetailScreen;
