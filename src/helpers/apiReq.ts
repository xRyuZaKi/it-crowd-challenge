import axios from "axios";
import { backend_url } from "./urls";

const apiReq = axios.create({
	baseURL: backend_url
});

export default apiReq;