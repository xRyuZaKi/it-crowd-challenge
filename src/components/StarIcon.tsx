import { AntDesign } from "@expo/vector-icons";
import { Colors } from "../styles";

interface StarProps {
  star: boolean;
}

export const StarIcon = ({ star }: StarProps) => (
  <AntDesign
    size={24}
    name={star ? "star" : "staro"}
    color={star ? Colors.starColor : Colors.textColor}
  />
);
