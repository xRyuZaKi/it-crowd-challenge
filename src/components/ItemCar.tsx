import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, Pressable, Text, View } from "react-native";
import { backend_url, urls } from "../helpers/urls";
import { useScreenDimensions } from "../hooks/useScreenDimensions";
import { Item } from "../models/CarInterface";
import { styles } from "../screens/Garage/styles";
import { StarIcon } from "./StarIcon";

interface Props {
  item: Item;
  checkFavorite: (index: number) => void;
  isFavoriteCar: (id: string) => boolean;
  index: number;
}

export const ItemCar = ({
  item,
  checkFavorite,
  isFavoriteCar,
  index,
}: Props) => {
  const navigation = useNavigation();
  const size = useScreenDimensions();

  const { image, model, id, make, year } = item;

  return (
    <View style={styles.card}>      
      <Pressable onPress={() => navigation.navigate("DetailScreen", item)}>
        <Image
          source={{
            uri: `${urls.image(image.url)}`,
          }}
          style={{
            width: "100%",
            height: size.width * 0.5,
          }}
        />
      </Pressable>
      <View style={styles.details}>
        <View style={styles.header}>
          <Text style={styles.model}>{model}</Text>
          <Pressable onPress={() => checkFavorite(index)}>
            <StarIcon star={isFavoriteCar(id)} />
          </Pressable>
        </View>
        <View style={styles.line} />
        <Text style={styles.makeYear}>
          {make} | {year}
        </Text>
      </View>
    </View>
  );
};
