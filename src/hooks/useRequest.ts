import {AxiosRequestConfig} from 'axios';
import React, {useState} from 'react';
import apiReq from '../helpers/apiReq';

export const useRequest = <T extends object>() => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [data, setData] = useState<T | any>({});
  const [hasError, setHasError] = useState<boolean>(false);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);

  const sendRequest = async (endPoint: string, config: AxiosRequestConfig) => {
    try {
      setIsLoading(true);
      setHasError(false);
      setIsSuccess(false);

      const response = await apiReq(endPoint, config);
      response?.status === 200 ? setIsSuccess(true) : setIsSuccess(false);

      setData(response.data);
    } catch (error) {
      setHasError(true);
      setIsSuccess(false);
    }
    setIsLoading(false);
  };

  const api = {sendRequest};

  return {isLoading, data, hasError, isSuccess, api};
};
