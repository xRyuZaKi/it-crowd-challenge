import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Item } from "../../src/models/CarInterface";

export const carsSlice = createSlice({
    name: "cars",
    initialState: {
        cars: [] as Item[]
    }, 
    reducers :{
        checkFavorite: (state, action: PayloadAction<Item[]>) => {
            state.cars = action.payload
        }
    }
});

export const {checkFavorite} = carsSlice.actions;

export default carsSlice.reducer;